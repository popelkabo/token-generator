﻿$SaltTicket = 'd954e28220d8895d562f9435072573d2'
$Hostname = 'my_hostname'

$cmd = "python $Root\hash.py'"
# ---------------------------
Write-Host "============================"
Write-Host "Script started."
Write-Host "Salt Ticket : $SaltTicket"
Write-Host "Hostname    : $Hostname"
Write-Host "============================"
try{
    Write-Host 'Generating token...'
    $hash = python 'D:\w\upw\token_generator\hash.py' $SaltTicket $Hostname
    Write-Host 'Ticket Number : ' -NoNewLine
    Write-Host $hash -ForegroundColor Green
    $IsSuccess = $True
}
catch{
    Write-Host 'Error encountered: ' -NoNewline
    Write-Host $Error[0].Exception.Message -ForegroundColor Red
    Write-Log "Error encountered: " -NoNewLine
    $IsSuccess = $false
}
