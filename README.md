# Token Generator PowerShell
---
## How to use
1. Open your `commandline` and change to your `working directory`

    ```bash
    cd C:\working\directory\
    ```
3. Clone this repository using [Git](https://git-scm.com/).

    ```bash
    git clone https://gitlab.com/melcdn/token-generator.git
    ```
4. Change to the `token generator` directory.
    ```bash
    cd token-generator
    ```
5. Open `token_generator.ps1` using `powershell_ise`.
    ```bash
    powershell_ise token_generator.ps1
    ```
6. Make sure you have `python` configured on your `ENVIRONMENT` by running below command in the `terminal pane` of `PowerShell ISE`. If not, check this [link](https://www.liquidweb.com/kb/how-do-i-set-system-variable-path-for-python-on-windows/).

    ![Script Pane](/images/env-python.png)
7. Open the `token_generator.ps1` on `PowerShell ISE`.
8. On the `script pane`, Put `$SaltTicket` and `$Hostname` values you have.

    ![Script Pane](/images/variables.png)
9. `CTRL+S` to `save` and `F5` to `run` it.

    ![Script Pane](/images/run.png)